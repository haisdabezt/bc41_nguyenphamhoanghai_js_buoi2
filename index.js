// Bài 1
/*
Đầu vào : số ngày đã làm 

Lương = số ngày * lương 1 ngày

Đầu ra : tổng số tiền lương 
*/
function tongluong() {
  var luong1ngay = document.getElementById("luong1ngay").value;
  var songaylam = document.getElementById("songaylam").value;
  var tongluong = "";
  tongluong = luong1ngay * songaylam;
  document.getElementById("Tongluong").innerHTML = tongluong.toLocaleString();
}

// Bài 2
/*
Đầu vào : 5 số thực

Giá trị trung bình = tổng 5 số thực / 5

Đầu ra : Giá trị trung bình của 5 số
*/

function ketqua() {
  var number1 = document.getElementById("number1").value;
  var number2 = document.getElementById("number2").value;
  var number3 = document.getElementById("number3").value;
  var number4 = document.getElementById("number4").value;
  var number5 = document.getElementById("number5").value;
  var trungbinhcong = "";
  trungbinhcong =
    (number1 * 1 + number2 * 1 + number3 * 1 + number4 * 1 + number5 * 1) / 5;
  document.getElementById("tbc").innerHTML = trungbinhcong.toLocaleString();
}

// Bài 3
/*
Đầu vào : số tiền USD 

tổng số tiền VND cần quy đổi = số tiền USD * 23.500

Đầu ra : tổng số tiền VND đã quy đổi
*/

var giatridola = 23500;

function quiDoi() {
  var input = document.getElementById("Dola");
  var output = "";
  output = input.value * giatridola;
  document.getElementById("ketquahienthi").innerHTML = output.toLocaleString();
}

// Bài 4
/* 
Đầu vào : chiều dài và chiều rộng 

Diện tích = chiều dài * chiều rộng
Chu vi = ( chiều dài + chiều rộng ) * 2

Đầu ra : Diện tích, chu vi 
*/

function ps() {
  var cd = document.getElementById("cd").value;
  var cr = document.getElementById("cr").value;
  var chuvi = "";
  chuvi = (cd + cr) * 2;
  document.getElementById("chuvi").innerHTML = chuvi.toLocaleString();
  var dientich = "";
  dientich = cd * cr;
  document.getElementById("dientich").innerHTML = dientich.toLocaleString();
}

// Bài 5
/*
Đầu vào : 1 số có 2 chữ số

Hàng chục = ký số / 10
Hàng đơn vị = ký số % 10 
Tổng kí số có 2 chữ số = hàng chục + hàng đơn vị

Đầu ra : tổng kí số có 2 chữ số
*/

function tong() {
  var so2chuso = document.getElementById("so2chuso").value;
  var a = Math.floor(so2chuso / 10);
  var b = so2chuso % 10;
  var tong = a + b;
  document.getElementById("tong").innerHTML = tong.toLocaleString();
}
